import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  TextInput,
  Platform,
  Image,
  Dimensions
} from 'react-native';
import {connect} from 'react-redux';
import MyIcon from 'react-native-vector-icons/Ionicons';
import ScalableText from 'react-native-text';


var WIDTH = Dimensions.get('window').width;
var HEIGHT = Dimensions.get('window').height;

const mapStateToProps = (state) => {
  return {

  }
}


const mapDispatchToProps = (dispatch) => {
  return {}
}


class CollectionTile extends React.Component {

  static defaultProps={
    collectionImage:'',
    collectionTopicTitle:'',
    collectionTopicStatus:'',
    titleText:{
      color:'black',
      fontWeight:'bold',
      fontSize:16,
      fontFamily:'PingFang SC'
    },
    typeText:{
      color:' d',
      fontSize:12,
      fontFamily:'PingFang SC',
    },
    imageLogo:'',
    isFavourite:false,
    collectionTouchableTile:{
        flex:1,
        borderWidth:1,
        width:(WIDTH-58)/2,
        borderRadius:10,
        overflow:'hidden',
        marginVertical:10,
        borderColor:'#d7d7d7',
        height:WIDTH*.425,
        elevation:1,
    },
    isFavouriteStyle:{
        position:'absolute',
        height:(WIDTH*.08),
        width:(WIDTH*.08),
        borderRadius:((WIDTH*.08)/2),
        backgroundColor:'#d8d8d8',
        top:8,
        right:6,
        justifyContent:'center',
        alignItems:'center'
      },
    isFavouriteIcon:'',
    triggerfunction:()=>{},
    imageStyle:{
      flex:1,
      width:((WIDTH-58)/2),
      overflow:'hidden'
    },
    noImageView:{
      flex:.7,
      width:((WIDTH-58)/2),
      backgroundColor:'#fb5aa9',
      justifyContent:'center',
      alignItems:'center'
  },
    noImageText:{
      color:'white',
      fontWeight:'bold',
      fontSize:16,
      fontFamily:'PingFang SC',
    },
    logoImageView:{},
    logoImageStyle:{},
    isFavouriteIconStyle:{}
  }

  constructor(props) {
    super(props);
  }

  componentDidMount(){
  }

  componentWillMount(){
  }

  render(){
    let {
      collectionImage,
      collectionTopicTitle,
      collectionTopicStatus,
      titleText,
      typeText,
      collectionNumber,
      imageLogo,
      isFavourite,
      collectionTouchableTile,
      isFavouriteStyle,
      isFavouriteIcon,
      triggerfunction,
      imageStyle,
      noImageView,
      noImageText,
      logoImageView,
      logoImageStyle,
      isFavouriteIconStyle
    }=this.props;
return(
  <TouchableOpacity style={collectionTouchableTile}
    onPress={()=>{
      triggerfunction()
    }}
  >

      {collectionImage ? (
        <View style={{flex:.7}}>
          <Image
            style={imageStyle}
            source={{uri:collectionImage}}
            resizeMode={'cover'}
           >
           </Image>

        </View>
        ) : (
          <View style={noImageView}>
            <ScalableText style={noImageText}>{collectionTopicTitle}</ScalableText>
          </View>
      )}

      {isFavourite? (
        <View style={isFavouriteStyle}>
         <MyIcon name={isFavouriteIcon} style={isFavouriteIconStyle}/>

        </View>

      ) : (
        <View/>
      )}
{(collectionTopicStatus!='' && collectionTopicStatus!=undefined) ? (
  <View style={{flex:.3,paddingLeft:7,paddingTop:3}}>
   <View><ScalableText style={titleText} numberOfLines={1} >{collectionTopicTitle}</ScalableText></View>
   <View style={{flex:1,flexDirection:'row'}}>
     <View style={{flex:3}}>
       <ScalableText style={typeText} numberOfLines={1}>{collectionTopicStatus}</ScalableText>
     </View>
     <View style={{flex:1,alignItems:'center'}}>
       <View style={logoImageView}>
         <Image
           style={logoImageStyle}
           source={{uri:imageLogo}}
           resizeMode={'cover'}
          >
         </Image>
        </View>
     </View>
   </View>

  </View>
) : (
   <View style={{justifyContent:'center',alignItems:'center',flex:.3}}><ScalableText style={titleText} numberOfLines={1} >{collectionTopicTitle}</ScalableText></View>
)}


  </TouchableOpacity>
)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CollectionTile)
